/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.oxweek2;

import java.util.Scanner;

/**
 *
 * @author Acer
 */
public class XOweek2 {

    static String[][] board = {{"-", "-", "-",}, {"-", "-", "-",}, {"-", "-", "-",}};
    static String player = "X";
    static int row, col;

    public static void main(String[] args) {
        printWelcome();
        while (true) {
            printBorad();
            printTurn();
            intputRowCol();

            if (Winner()) {
                printBorad();
                printWin();
                Continue();
                break;

            } else if (Draw()) {
                printBorad();
                printDraw();
                Continue();
                break;
                
            }
            
            swichplayer();

        }
    }

    private static void printWelcome() {
        System.out.println("Welcome to OX!");
    }

    private static void printBorad() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(board[i][j] + " ");
            }
            System.out.println("");

        }

    }

    private static void printTurn() {
        System.out.println(player + " turn");
    }

    private static void intputRowCol() {
        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.println("Please input row,col (row [0-2] and column [0-2]): 0 2");
            row = sc.nextInt();
            col = sc.nextInt();
            if (board[row][col] == "-") {
                board[row][col] = player;
                break;
            }

        }
    }

    private static void printWin() {
        System.out.println(player + " wins");
    }

    private static boolean Winner() {
        if (checkRow()) {
            return true;
        } else if (checkCol()) {
            return true;
        } else if (checkDiagonal()) {
            return true;
        }

        return false;
    }

    private static boolean checkRow() {
        for (int i = 0; i < 3; i++) {
            if (board[row][i] != player) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkCol() {
        for (int i = 0; i < 3; i++) {
            if (board[i][col] != player) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkDiagonal() {
        if (board[0][0] == (player) && board[1][1] == (player) && board[2][2] == (player)) {
            return true;
        }
        if (board[0][2] == (player) && board[1][1] == (player) && board[2][0] == (player)) {
            return false;
        }
        return false;
    }

    private static void swichplayer() {
        if (player == "X") {
            player = "O";
        } else {
            player = "X";
        }
    }

    private static boolean Draw() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (board[i][j] == "-") {
                    return false;
                } 
                  
            }
        }
        return true;
    }

    private static void printDraw() {
        System.out.println("Draw!!");
    }

    private static void Continue() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Do you want to play again? (y/n): ");
        String playAgain = sc.next();
        
        if(playAgain.equalsIgnoreCase("y")){
        resetBoard();
        main(null);
        }else{
        
        }
        
    }
    private static void resetBoard(){
        board = new String[][]{{"-", "-", "-",}, {"-", "-", "-",}, {"-", "-", "-",}};
        player = "X";
        row = 0;
        col = 0;
    }
    
    

}
